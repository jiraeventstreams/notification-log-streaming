package com.atlassian.product.fabric.notifications;

import brave.propagation.Propagation;
import com.atlassian.notifications.common.EmailRecipient;
import com.atlassian.notifications.common.EmailRequest;
import com.atlassian.notifications.common.RenderedEmail;
import com.atlassian.notifications.jira.JiraEmailRequest;
import com.atlassian.product.fabric.notifications.client.distributor.ImmutableMessagePart;
import com.atlassian.product.fabric.notifications.client.distributor.ImmutableNotificationDistributorPayload;
import com.atlassian.product.fabric.notifications.client.distributor.NotificationDistributorPayload;
import com.atlassian.product.fabric.notifications.client.distributor.NotificationDistributorPayload.MessageType;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static com.atlassian.notifications.common.ContentType.HTML;
import static com.atlassian.product.fabric.notifications.client.distributor.NotificationDistributorPayload.Account.fromAtlassianId;
import static com.atlassian.product.fabric.notifications.client.distributor.NotificationDistributorPayload.Account.fromIdAndName;
import static com.atlassian.product.fabric.notifications.client.distributor.NotificationDistributorPayload.Channel.EMAIL;
import static com.atlassian.product.fabric.notifications.client.distributor.NotificationDistributorPayload.NotificationType.GROUPED;
import static com.atlassian.product.fabric.notifications.client.distributor.NotificationDistributorPayload.NotificationType.PRE_RENDERED;

public class NotificationDistributorPayloadBuilder {
    private static final Propagation.Setter<Map<String, String>, String> PROPAGATION_SETTER = Map::put;

    public NotificationDistributorPayloadBuilder() {
    }

    public NotificationDistributorPayload createGroupedPayload(EmailRequest request) {
        return createPayloadWithContext(request, GROUPED, EMAIL);
    }

    public NotificationDistributorPayload createPreRenderedPayload(EmailRequest request) {
        ImmutableMessagePart.Builder notificationMessagePart = ImmutableMessagePart.builder()
                .timestamp(request.getTimestamp())
                .eventType(request.getType())
                .objectId(request.getResource())
                .renderedEmail(request.getRenderedEmail())
                .emailRequestId(request.getEmailRequestId())
                .context(null);

        if (request.getActioner() != null) {
            notificationMessagePart.addActioners((fromAtlassianId(request.getActioner())));
        }

        final EmailRecipient emailRecipient = request.getEmailRecipient();
        return ImmutableNotificationDistributorPayload.builder()
                .tracing(createTracingContext(request))
                .notificationType(PRE_RENDERED)
                .recipient(fromAtlassianId(emailRecipient.getAccountId()))
                .channel(EMAIL)
                .messageType(request.getContentType().equals(HTML) ? MessageType.HTML : MessageType.TEXT)
                .customFrom(request.getCustomFrom())
                .addMessageParts(notificationMessagePart.build())
                .cloudUrl(request.getHostname())
                .dryRunMode(request.isDryRunMode())
                .locale(emailRecipient.getLocale())
                .timeZone(emailRecipient.getTimeZone())
                .build();
    }

    Map<String, String> createTracingContext() {
        return createTracingContext(null);
    }

    private Map<String, String> createTracingContext(@Nullable final EmailRequest request) {
        final Map<String, String> tracingContextMap = new HashMap<>();

        Optional.ofNullable(request)
                .map(EmailRequest::getRenderedEmail)
                .map(RenderedEmail::getAtlOriginHash)
                .ifPresent(s -> tracingContextMap.put(NotificationDistributorPayload.ATL_ORIGIN_HASH_HEADER_KEY, s));

        return tracingContextMap;
    }

    private NotificationDistributorPayload createPayloadWithContext(EmailRequest request,
                                                                    NotificationDistributorPayload.NotificationType type,
                                                                    NotificationDistributorPayload.Channel channel) {
        ImmutableMessagePart.Builder notificationMessagePart = ImmutableMessagePart.builder()
                .timestamp(request.getTimestamp())
                .eventType(request.getType())
                .objectId(request.getResource())
                .emailRequestId(request.getEmailRequestId())
                .context(request.getRequestContext());

        if (request.getActioner() != null) {
            String actionerName = null;
            if (request instanceof JiraEmailRequest) {
                actionerName = ((JiraEmailRequest) request).getActionerFullName();
            }
            notificationMessagePart.addActioners(fromIdAndName(request.getActioner(), actionerName));
        }

        return ImmutableNotificationDistributorPayload.builder()
                .tracing(createTracingContext(request))
                .notificationType(type)
                .recipient(fromAtlassianId(request.getEmailRecipient().getAccountId()))
                .channel(channel)
                .messageType(MessageType.HTML)
                .customFrom(request.getCustomFrom())
                .resourceCreationTime(request.getResourceCreationTime())
                .addMessageParts(notificationMessagePart.build())
                .cloudUrl(request.getHostname())
                .dryRunMode(request.isDryRunMode())
                .locale(request.getEmailRecipient().getLocale())
                .timeZone(request.getEmailRecipient().getTimeZone())
                .build();
    }
}
