package com.atlassian.product.fabric.notifications;

import com.atlassian.notifications.jira.JiraEmailRequest;
import org.apache.flink.api.common.functions.AggregateFunction;

import java.util.LinkedList;
import java.util.List;

public class JiraEmailRequestListListAggregateFunction implements
        AggregateFunction<JiraEmailRequest, List<JiraEmailRequest>, List<JiraEmailRequest>> {

    @Override
    public List<JiraEmailRequest> createAccumulator() {
        return new LinkedList<>();
    }

    @Override
    public List<JiraEmailRequest> add(JiraEmailRequest value, List<JiraEmailRequest> accumulator) {
        accumulator.add(value);
        return accumulator;
    }

    @Override
    public List<JiraEmailRequest> getResult(List<JiraEmailRequest> accumulator) {
        return accumulator;
    }

    @Override
    public List<JiraEmailRequest> merge(List<JiraEmailRequest> a, List<JiraEmailRequest> b) {
        a.addAll(b);
        return a;
    }
}
