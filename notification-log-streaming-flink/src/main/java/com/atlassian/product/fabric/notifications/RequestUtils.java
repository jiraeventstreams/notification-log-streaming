package com.atlassian.product.fabric.notifications;


import com.atlassian.notifications.common.EmailRequest;
import com.atlassian.product.fabric.notifications.client.distributor.ImmutableBatchingMetadata;
import com.atlassian.product.fabric.notifications.client.distributor.ImmutableNotificationDistributorPayload;
import com.atlassian.product.fabric.notifications.client.distributor.MessagePart;
import com.atlassian.product.fabric.notifications.client.distributor.NotificationDistributorPayload;
import com.atlassian.product.fabric.notifications.email.repository.DBDirectEmailRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.guava.GuavaModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.io.IOException;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static com.atlassian.product.fabric.notifications.client.distributor.NotificationDistributorPayload.NotificationType.GROUPED;
import static com.atlassian.product.fabric.notifications.util.StringCompressor.compressString;
import static com.atlassian.product.fabric.notifications.util.StringCompressor.uncompressString;
import static com.fasterxml.jackson.databind.DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE;
import static com.fasterxml.jackson.databind.SerializationFeature.WRITE_DATES_AS_TIMESTAMPS;

public class RequestUtils {

    private static NotificationDistributorPayloadBuilder payloadBuilder = new NotificationDistributorPayloadBuilder();

    public static final ObjectMapper objectMapper = new ObjectMapper()
            .registerModule(new GuavaModule())
            .registerModule(new JavaTimeModule())
            .configure(ADJUST_DATES_TO_CONTEXT_TIME_ZONE, false)
            .configure(WRITE_DATES_AS_TIMESTAMPS, false);


    public static NotificationDistributorPayload deserializePayload(DBDirectEmailRequest emailRequest) {
        String json = uncompressString(emailRequest.getDistributorPayload());
        try {
            return objectMapper.readValue(json, NotificationDistributorPayload.class);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static byte[] convertEmail(EmailRequest emailRequest) {
        NotificationDistributorPayload payload = payloadBuilder.createGroupedPayload(emailRequest);
        try {
            final String json = objectMapper.writeValueAsString(payload);
            final byte[] compressedPayload = compressString(json);
            return compressedPayload;
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }


    private static NotificationDistributorPayload toStreamNotificationDistributorPayload(List<DBDirectEmailRequest> streamPayloadDBS) {

        final DBDirectEmailRequest firstEmail = streamPayloadDBS.get(0);
        NotificationDistributorPayload firstPayload = deserializePayload(firstEmail);

        List<MessagePart> messageParts = streamPayloadDBS.stream()
                .map(RequestUtils::deserializePayload)
                .flatMap(notificationDistributorPayload -> notificationDistributorPayload.getMessageParts().stream())
                .collect(Collectors.toList());

        NotificationDistributorPayload payload = ImmutableNotificationDistributorPayload
                .copyOf(firstPayload)
                .withBatchingMetadata(getBatchingMetadata(firstEmail))
                .withMessageParts(messageParts)
                .withNotificationType(GROUPED);
        return payload;
    }

    private static ImmutableBatchingMetadata getBatchingMetadata(DBDirectEmailRequest firstEmail) {
        return ImmutableBatchingMetadata.builder()
                .duration(Duration.between(firstEmail.getEmailTimestamp(), ZonedDateTime.now()))
                .build();
    }


}
