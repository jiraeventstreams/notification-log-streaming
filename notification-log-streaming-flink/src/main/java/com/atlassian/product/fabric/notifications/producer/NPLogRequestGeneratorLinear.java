package com.atlassian.product.fabric.notifications.producer;

import com.atlassian.notifications.jira.ImmutableJiraEmailRequest;
import com.atlassian.notifications.jira.JiraEmailRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.guava.GuavaModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Map;

import static com.fasterxml.jackson.databind.DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE;
import static com.fasterxml.jackson.databind.SerializationFeature.WRITE_DATES_AS_TIMESTAMPS;

public class NPLogRequestGeneratorLinear {

    private static ObjectMapper objectMapper = new ObjectMapper()
            .registerModule(new JavaTimeModule())
            .registerModule(new GuavaModule())
            .configure(ADJUST_DATES_TO_CONTEXT_TIME_ZONE, false)
            .configure(WRITE_DATES_AS_TIMESTAMPS, false);

    public static void main(String[] args) throws JsonProcessingException, InterruptedException {
        Producer<String, String> producer = getKafkaProducer();

        producer.send(getProducerRecord(1, 1, "1"));
        producer.send(getProducerRecord(1, 1, "2"));
        producer.send(getProducerRecord(1, 1, "3"));
        producer.send(getProducerRecord(1, 1, "4"));
        producer.send(getProducerRecord(1, 1, "5"));
        producer.send(getProducerRecord(1, 1, "6"));
        producer.send(getProducerRecord(1, 1, "7"));
        producer.send(getProducerRecord(1, 1, "8"));
        producer.send(getProducerRecord(1, 1, "9"));
        producer.send(getProducerRecord(1, 1, "10"));

        producer.flush();
        Thread.sleep(2500);

        producer.send(getProducerRecord(1, 1, "11"));
        producer.send(getProducerRecord(1, 1, "12"));

        producer.flush();
        producer.close();
    }


    private static Producer<String, String> getKafkaProducer() {
        Map<String, Object> configs = getKafkaConfig();

        return new KafkaProducer<>(configs);
    }

    public static Map<String, Object> getKafkaConfig() {
        Map<String, Object> configs = new HashMap<>();
        configs.put("key.serializer", StringSerializer.class);
        configs.put("value.serializer", StringSerializer.class);
        configs.put("bootstrap.servers", "localhost:9092");
        configs.put("group.id", "npLogConsumer");

        //configs.put("buffer.memory", 33554432l);
        configs.put("acks", "all");
        return configs;
    }

    public static ProducerRecord<String, String> getProducerRecord(int issueId, int recipientId, String emailRequestId) throws JsonProcessingException {
        ImmutableJiraEmailRequest.Builder requestBuilder =
                JiraEmailRequestBuildersFixture.getJiraIssueAssignedEmailRequestBuilder(
                        recipientId,
                        issueId,
                        ZonedDateTime.now()
                );
        requestBuilder.emailRequestId(emailRequestId);

        JiraEmailRequest request = requestBuilder.build();

        String key = recipientId + "-" + issueId;
        String json = objectMapper.writeValueAsString(request);

        return new ProducerRecord<>("nplog", key, json);
    }
}
