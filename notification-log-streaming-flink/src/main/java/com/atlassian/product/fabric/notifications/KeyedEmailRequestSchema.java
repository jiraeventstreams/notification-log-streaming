package com.atlassian.product.fabric.notifications;

import com.atlassian.notifications.jira.JiraEmailRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.guava.GuavaModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.streaming.util.serialization.KeyedDeserializationSchema;

import java.io.IOException;

import static com.fasterxml.jackson.databind.DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE;
import static com.fasterxml.jackson.databind.SerializationFeature.WRITE_DATES_AS_TIMESTAMPS;


/**
 * Deserialize the message into a JiraEmailRequest
 */
public class KeyedEmailRequestSchema implements KeyedDeserializationSchema<JiraEmailRequest> {

    public static final ObjectMapper objectMapper = new ObjectMapper()
            .registerModule(new GuavaModule())
            .registerModule(new JavaTimeModule())
            .configure(ADJUST_DATES_TO_CONTEXT_TIME_ZONE, false)
            .configure(WRITE_DATES_AS_TIMESTAMPS, false);

    @Override
    public JiraEmailRequest deserialize(byte[] messageKey, byte[] message, String topic, int partition, long offset) throws IOException {
        return objectMapper.readValue(message, JiraEmailRequest.class);
    }

    @Override
    public boolean isEndOfStream(JiraEmailRequest nextElement) {
        return false;
    }

    @Override
    public TypeInformation<JiraEmailRequest> getProducedType() {
        return TypeInformation.of(JiraEmailRequest.class);
    }
}
