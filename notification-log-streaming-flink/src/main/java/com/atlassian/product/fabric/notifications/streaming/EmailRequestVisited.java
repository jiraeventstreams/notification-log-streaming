package com.atlassian.product.fabric.notifications.streaming;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.UUID;

public class EmailRequestVisited implements Serializable {

    public enum Channel {
        SLACK, MAIL, JIRA_APP, JIRA_WEB;
    }

    private String accountId;

    private String issueKey;

    private String visualizationId;

    private long timestamp;

    private Channel channel;

    public EmailRequestVisited() {
    }

    public EmailRequestVisited(String accountId, String issueKey, Channel channel) {
        this.accountId = accountId;
        this.issueKey = issueKey;
        this.channel = channel;
        this.visualizationId = UUID.randomUUID().toString();
        this.timestamp = ZonedDateTime.now().toInstant().toEpochMilli();
    }

    public String getAccountId() {
        return accountId;
    }

    public String getIssueKey() {
        return issueKey;
    }

    public Channel getChannel() {
        return channel;
    }

    public String getVisualizationId() {
        return visualizationId;
    }

    public long getTimestamp() {
        return timestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmailRequestVisited that = (EmailRequestVisited) o;
        return Objects.equals(accountId, that.accountId) &&
                Objects.equals(issueKey, that.issueKey);
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountId, issueKey);
    }
}
