package com.atlassian.product.fabric.notifications;

import com.atlassian.notifications.jira.JiraEmailRequest;
import com.atlassian.product.fabric.notifications.email.EmailDispatchStatus;
import com.atlassian.product.fabric.notifications.email.repository.DBDirectEmailRequest;
import com.atlassian.product.fabric.notifications.streaming.EmailRequestVisited;
import org.apache.flink.api.common.functions.CoGroupFunction;
import org.apache.flink.api.common.serialization.DeserializationSchema;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.sink.SinkFunction;
import org.apache.flink.streaming.api.windowing.assigners.ProcessingTimeSessionWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.flink.util.Collector;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;
import java.util.TreeMap;
import java.util.stream.Collectors;

import static com.atlassian.product.fabric.notifications.RequestUtils.convertEmail;

public class StreamingJob {

    public static void main(String[] args) throws Exception {
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        SerializerReg.registerSerializers(env);

        //Initialize a Kafka consumer pointing to the topic that will receive the notifications
        FlinkKafkaConsumer<JiraEmailRequest> kafkaNplog =
                getKafkaConsumer("nplog", new EmailRequestSchema());

        //Initialize a Kafka consumer pointing to the topic that will the notification visited events
        FlinkKafkaConsumer<EmailRequestVisited> kafkaVisited =
                getKafkaConsumer("nplogvisited", new VisitedSchema());

        //set a stream based on the notification request stream
        KeyedStream<JiraEmailRequest, String> requestStream = env.addSource(kafkaNplog)
                                                              .keyBy(StreamingJob::getKey);

        //sinks all notification requests to the notification respository / drawer.
        requestStream.addSink(sinkToNotificationRepository());

        //set a stream based on the notification visited event topic
        KeyedStream<EmailRequestVisited, String> visitedStream = env.addSource(kafkaVisited)
                                                                .keyBy(StreamingJob::getKey);

        // "left outer joins" the request stream and the related notification visited event stream in a time window.
        DataStream<List<RequestVisited>> joined = requestStream
            .coGroup(visitedStream)
            // "on" clause of the left outer join. Compares accountId + IssueKey
            .where(    r -> r.getEmailRecipient().getAccountId()   +    r.getRequestContext().getIssue().getKey())
            .equalTo(  v -> v.getAccountId()                       +    v.getIssueKey())
            // Session window definition.
            .window(ProcessingTimeSessionWindows.withGap(Time.seconds(2)))
            // implements what to do with the requests and visualisation events found in the same window
            // it excludes the Notification Requests that were already visited in different channel.
            .apply(leftJoinFunction());

        //Updates the notification repository
        joined.addSink(updateNotificationRepostory());

        //Sends the batching result to the Notification Distributor
        joined.addSink(sendToDistributorBySQS());

        // execute program
        env.execute("Flink Streaming Java API Skeleton");
    }

    private static SinkFunction<JiraEmailRequest> sinkToNotificationRepository() {
        return new SinkFunction<JiraEmailRequest>() {
            @Override
            public void invoke(JiraEmailRequest value, Context context) throws Exception {
                System.out.println("SAVING THE INDIVIDUAL REQUEST, " + value.getEmailRequestId() + " IN THE DRAWER!");
            }
        };
    }

    private static SinkFunction<List<RequestVisited>> updateNotificationRepostory() {
        return new SinkFunction<List<RequestVisited>>() {
            @Override
            public void invoke(List<RequestVisited> value, Context context) throws Exception {
                System.out.println("SAVING THE BATCHING RESULT OF , " + value.size() + " REQUESTS IN THE DRAWER!");
            }
        };
    }

    /**
     * A Dummy sink functions to show the batched messages
     *
     * @return
     */
    private static SinkFunction<List<RequestVisited>> sendToDistributorBySQS() {
        return new SinkFunction<List<RequestVisited>>() {
            @Override
            public void invoke(List<RequestVisited> requests, Context context) {

                requests.sort(Comparator.comparingLong(r -> r.getJiraEmailRequest().getTimestamp().toInstant().toEpochMilli()));

                StringBuffer buffer = new StringBuffer();

                if (!requests.isEmpty()) {

                    JiraEmailRequest jiraEmailRequest = requests.get(0).getJiraEmailRequest();
                    buffer.append("\n\nSENDING TO DISTRIBUTOR, batch for accountId " + jiraEmailRequest.getEmailRecipient().getAccountId() + " issueKey: " + jiraEmailRequest.getRequestContext().getIssue().getKey());
                    for (RequestVisited request : requests) {
                        Optional<EmailRequestVisited> optVisited = request.visiteds;
                        buffer.append(
                                " \n ---> requestId: " + request.getJiraEmailRequest().getEmailRequestId() +
                                        " accountId: " + request.getJiraEmailRequest().getEmailRecipient().getAccountId() +
                                        " issueKey: " + request.getJiraEmailRequest().getRequestContext().getIssue().getKey() +
                                        " at: " + request.getJiraEmailRequest().getTimestamp().toInstant().toEpochMilli()
                        );

                        buffer.append(
                                optVisited.map(visited -> {
                                    if (visited.getTimestamp() > request.getJiraEmailRequest().getTimestamp().toInstant().toEpochMilli()) {
                                        return (" Visited in Channel " + visited.getChannel() + " at " + visited.getTimestamp());
                                    }
                                    return (" Visited in Channel " + visited.getChannel() + " BUT BEFORE THE NOTIFICATION at " + visited.getTimestamp());
                                }).orElse(" NOT VISITED AT ALL")
                        );


                    }
                    buffer.append("\n");

                    System.out.println(buffer.toString());
                }
            }
        };
    }

    private static CoGroupFunction<JiraEmailRequest, EmailRequestVisited, List<RequestVisited>> leftJoinFunction() {

        return new CoGroupFunction<JiraEmailRequest, EmailRequestVisited, List<RequestVisited>>() {
            @Override
            public void coGroup(Iterable<JiraEmailRequest> first, Iterable<EmailRequestVisited> second, Collector<List<RequestVisited>> collector) {

                Map<String, EmailRequestVisited> mapVisited = new TreeMap<>();
                for (EmailRequestVisited visited : second) {
                    mapVisited.merge(getKey(visited), visited, (v1, v2) -> v1.getTimestamp() > v2.getTimestamp() ? v1 : v2);
                }

                List<RequestVisited> visiteds = new LinkedList<>();
                for (JiraEmailRequest request : first) {
                    visiteds.add(new RequestVisited(request, Optional.ofNullable(mapVisited.get(getKey(request)))));

                }

                if (!visiteds.isEmpty())
                    collector.collect(visiteds);
                //collector.close();

            }
        };
    }

    private static String getKey(EmailRequestVisited v) {
        return v.getAccountId() + v.getIssueKey();
    }

    private static String getKey(JiraEmailRequest r) {
        return r.getEmailRecipient().getAccountId() + r.getRequestContext().getIssue().getKey();
    }

    private static SinkFunction<List<DBDirectEmailRequest>> getSinkFunction() {
        return new SinkFunction<List<DBDirectEmailRequest>>() {
            public void invoke(List<DBDirectEmailRequest> emailRequestBatch, Context context) {
                printBatched(emailRequestBatch);
            }
        };
    }

    private static List<DBDirectEmailRequest> convert(List<JiraEmailRequest> listReq) {
        return listReq.stream().map(req ->
                new DBDirectEmailRequest(req, EmailDispatchStatus.DISPATCHED, convertEmail(req))).collect(Collectors.toList());
    }

    private static <T> FlinkKafkaConsumer<T> getKafkaConsumer(String topicName, DeserializationSchema<T> schema) {
        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers", "localhost:9092");
        properties.setProperty("group.id", "npLogConsumerFlink");

        return new FlinkKafkaConsumer<>(
                topicName,
                schema,
                properties);
    }

    private static void printBatched(List<DBDirectEmailRequest> emailRequestBatch) {
        String msg = "\n\n BATCHED MESSAGES, SIZE " + emailRequestBatch.size() + "\n";
        String req = emailRequestBatch.stream().map(id ->
                " recipient: " + id.getAccountId() +
                        " AVI: " + id.getAVI()
        ).collect(Collectors.joining("\n"));
        System.out.println(msg + req);
    }

    public static class RequestVisited {
        private JiraEmailRequest jiraEmailRequest;
        private Optional<EmailRequestVisited> visiteds;

        public RequestVisited() {
        }

        public RequestVisited(JiraEmailRequest jiraEmailRequest, Optional<EmailRequestVisited> visiteds) {
            this.jiraEmailRequest = jiraEmailRequest;
            this.visiteds = visiteds;
        }

        public RequestVisited(JiraEmailRequest jiraEmailRequest) {
            this(jiraEmailRequest, null);
        }

        public JiraEmailRequest getJiraEmailRequest() {
            return jiraEmailRequest;
        }

        public Optional<EmailRequestVisited> getVisiteds() {
            return visiteds;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            RequestVisited visited = (RequestVisited) o;
            return Objects.equals(jiraEmailRequest, visited.jiraEmailRequest) &&
                    Objects.equals(visiteds, visited.visiteds);
        }

        @Override
        public int hashCode() {
            return Objects.hash(jiraEmailRequest, visiteds);
        }
    }

}
