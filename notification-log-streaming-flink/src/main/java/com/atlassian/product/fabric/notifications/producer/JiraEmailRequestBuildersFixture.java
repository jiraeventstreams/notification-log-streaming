package com.atlassian.product.fabric.notifications.producer;

import com.atlassian.ari.ARI;
import com.atlassian.avi.AVI;
import com.atlassian.notifications.common.ContentType;
import com.atlassian.notifications.common.ImmutableEmailAddress;
import com.atlassian.notifications.common.ImmutableEmailAddress.Builder;
import com.atlassian.notifications.common.ImmutableEmailRecipient;
import com.atlassian.notifications.common.ImmutableInlineImage;
import com.atlassian.notifications.common.ImmutableRenderedEmail;
import com.atlassian.notifications.common.InlineImage;
import com.atlassian.notifications.jira.ImmutableJiraEmailRequest;
import com.atlassian.notifications.jira.ImmutableJiraIssue;
import com.atlassian.notifications.jira.ImmutableJiraIssueFields;
import com.atlassian.notifications.jira.ImmutableJiraIssueStatus;
import com.atlassian.notifications.jira.ImmutableJiraIssueStatusCategory;
import com.atlassian.notifications.jira.ImmutableJiraIssueType;
import com.atlassian.notifications.jira.ImmutableJiraProject;
import com.atlassian.notifications.jira.ImmutableJiraRequestContext;
import com.atlassian.notifications.jira.JiraIssueStatusCategoryKey;
import com.atlassian.notifications.jira.assigned.ImmutableIssueAssignedRequestContext;
import com.atlassian.notifications.jira.assigned.IssueAssignedRequestContext;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;

import java.time.ZonedDateTime;
import java.util.Locale;

import static java.time.ZoneOffset.UTC;
import static java.util.TimeZone.getTimeZone;

public final class JiraEmailRequestBuildersFixture {

    private JiraEmailRequestBuildersFixture() {
    }


    public static ImmutableJiraEmailRequest.Builder getJiraIssueAssignedEmailRequestBuilder(Integer recipientId, Integer issueId, ZonedDateTime timestamp) {

        IssueAssignedRequestContext build = getJiraIssueAssignedContextBuilder("ISS-" + issueId).build();

        return ImmutableJiraEmailRequest.builder()
                .actionerFullName("John Wick")
                .actioner("3530a6c7-b09e-4af8-ac9d-1e5db3444see")
                .customFrom(getEmailAddress().build())
                .dryRunMode(false)
                .emailRecipient(getEmailRecipientBuilder("recipient" + recipientId).build())
                .hostname("xyz.atlassian.com")
                .type(AVI.from("avi:jira:created:issue"))
                .resource(ARI.parse("ari:cloud:jira:00000000-0000-0000-0000-000000000000:issue/" + issueId))
                .contentType(ContentType.HTML)
                .timestamp(timestamp)
                .requestContext(build)
                .renderedEmail(getRenderedEmailBuilder().build())
                .xJiraFingerPrint("xJiraFingerPrintValue");

    }

    public static ImmutableJiraEmailRequest.Builder getJiraIssueAssignedEmailRequestBuilder() {

        IssueAssignedRequestContext build = getJiraIssueAssignedContextBuilder().build();

        return ImmutableJiraEmailRequest.builder()
                .actionerFullName("John Wick")
                .actioner("3530a6c7-b09e-4af8-ac9d-1e5db3444see")
                .customFrom(getEmailAddress().build())
                .dryRunMode(false)
                .emailRecipient(getEmailRecipientBuilder().build())
                .hostname("xyz.atlassian.com")
                .type(AVI.from("avi:jira:event:issue"))
                .resource(ARI.parse("ari:cloud:jira:00000000-0000-0000-0000-000000000000:issue/1"))
                .contentType(ContentType.TEXT)
                .timestamp(ZonedDateTime.now())
                .requestContext(build)
                .renderedEmail(getRenderedEmailBuilder().build())
                .xJiraFingerPrint("xJiraFingerPrintValue");

    }

    public static ImmutableJiraEmailRequest.Builder getJiraEmailRequestBuilder() {
        return ImmutableJiraEmailRequest.builder()
                .actionerFullName("John Wick")
                .actioner("3530a6c7-b09e-4af8-ac9d-1e5db3444see")
                .customFrom(getEmailAddress().build())
                .dryRunMode(false)
                .emailRecipient(getEmailRecipientBuilder().build())
                .hostname("xyz.atlassian.com")
                .type(AVI.from("avi:jira:event:issue"))
                .resource(ARI.parse("ari:cloud:jira:00000000-0000-0000-0000-000000000000:issue/1"))
                .contentType(ContentType.TEXT)
                .timestamp(ZonedDateTime.now())
                .requestContext(getJiraContextBuilder().build())
                .renderedEmail(getRenderedEmailBuilder().build())
                .xJiraFingerPrint("xJiraFingerPrintValue");
    }

    private static Builder getEmailAddress() {
        return ImmutableEmailAddress.builder()
                .address("jira@xyz.atlassian.com")
                .name("jira");
    }

    public static ImmutableIssueAssignedRequestContext.Builder getJiraIssueAssignedContextBuilder() {

        return getJiraIssueAssignedContextBuilder("ISS-10");

    }

    private static ImmutableIssueAssignedRequestContext.Builder getJiraIssueAssignedContextBuilder(String key) {
        return ImmutableIssueAssignedRequestContext.builder()
                .actioner("actioner value")
                .oldAssignee("bob")
                .newAssignee("ana")
                .fingerPrint("fingerprint-value")
                .issue(getJiraIssueBuilder(key).build())
                .eventTime(ZonedDateTime.parse("2007-12-03T10:15:20+01:00"));
    }

    public static ImmutableJiraRequestContext.Builder getJiraContextBuilder() {
        return ImmutableJiraRequestContext.builder()
                .actioner("actioner value")
                .fingerPrint("fingerprint-value")
                .issue(getJiraIssueBuilder().build())
                .eventTime(ZonedDateTime.parse("2007-12-03T10:15:20+01:00"));
    }

    public static ImmutableRenderedEmail.Builder getRenderedEmailBuilder() {
        Iterable<? extends InlineImage> images = Lists.newArrayList(
                ImmutableInlineImage.builder()
                        .contentId("myimgid")
                        .contentType("jpg")
                        .name("myimg.jpg")
                        .data("itshouldbeabinarycontent".getBytes())
                        .build(),
                ImmutableInlineImage.builder()
                        .contentId("myimgid2")
                        .contentType("jpg")
                        .name("myimg2.jpg")
                        .data("itshouldbeabinarycontent".getBytes())
                        .build()
        );

        return ImmutableRenderedEmail.builder()
                .subject("mail subject value")
                .body("the email body")
                .headers(ImmutableMap.of(
                        "X-JIRA-FingerPrint", "fingerprint",
                        "Auto-Submitted", "auto-generated",
                        "Precedence", "bulk",
                        "Message-ID", "message id value",
                        "In-Reply-To", "in reply to value"))
                .inlineImages(images)
                .atlOriginHash("");
    }

    public static ImmutableJiraIssue.Builder getJiraIssueBuilder() {
        String key = "ISS-10";

        return getJiraIssueBuilder(key);
    }

    private static ImmutableJiraIssue.Builder getJiraIssueBuilder(String key) {
        String viewUrl = "http://jira.localhost.atl-test.space:8080/jira/browse/" + key + "/";
        return ImmutableJiraIssue.builder()
                .key(key)
                .viewUrl("http://jira.localhost.atl-test.space:8080/jira/browse/" + key)
                .addCommentUrl(viewUrl + "#add-comment")
                .fields(getJiraIssueFieldsBuilder().build());
    }

    public static ImmutableEmailRecipient.Builder getEmailRecipientBuilder() {
        String accountId = "10001";
        return getEmailRecipientBuilder(accountId);
    }

    private static ImmutableEmailRecipient.Builder getEmailRecipientBuilder(String accountId) {
        return ImmutableEmailRecipient.builder()
                .accountId(accountId)
                .locale(Locale.US)
                .timeZone(getTimeZone(UTC));
    }

    public static ImmutableJiraIssueFields.Builder getJiraIssueFieldsBuilder() {
        return ImmutableJiraIssueFields.builder()
                .summary("issue summary")
                .issuetype(getJiraIssueTypeBuilder().build())
                .status(getJiraIssueStatusBuilder().build())
                .project(getJiraProjectBuilder().build());
    }

    public static ImmutableJiraIssueType.Builder getJiraIssueTypeBuilder() {
        return ImmutableJiraIssueType.builder()
                .name("Epic")
                .iconUrl("http://jira.localhost.atl-test.space:8080/jira/epicurl");
    }

    private static ImmutableJiraIssueStatus.Builder getJiraIssueStatusBuilder() {
        return ImmutableJiraIssueStatus.builder()
                .name("In Progress")
                .category(getJiraIssueStatusCategoryBuilder().build());
    }

    private static ImmutableJiraIssueStatusCategory.Builder getJiraIssueStatusCategoryBuilder() {
        return ImmutableJiraIssueStatusCategory.builder()
                .key(JiraIssueStatusCategoryKey.IN_PROGRESS);
    }

    public static ImmutableJiraProject.Builder getJiraProjectBuilder() {
        return ImmutableJiraProject.builder()
                .name("Test Project")
                .viewUrl("http://jira.localhost.atl-test.space:8080/jira/browse/TP");
    }
}
