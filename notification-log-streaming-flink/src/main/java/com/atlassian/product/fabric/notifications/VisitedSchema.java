package com.atlassian.product.fabric.notifications;

import com.atlassian.product.fabric.notifications.streaming.EmailRequestVisited;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.guava.GuavaModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.flink.api.common.serialization.DeserializationSchema;
import org.apache.flink.api.common.typeinfo.TypeInformation;

import java.io.IOException;

import static com.fasterxml.jackson.databind.DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE;
import static com.fasterxml.jackson.databind.SerializationFeature.WRITE_DATES_AS_TIMESTAMPS;


/**
 * Deserialize the message into a EmailRequestVisited
 */
public class VisitedSchema implements DeserializationSchema<EmailRequestVisited> {

    public static final ObjectMapper objectMapper = new ObjectMapper()
            .registerModule(new GuavaModule())
            .registerModule(new JavaTimeModule())
            .configure(ADJUST_DATES_TO_CONTEXT_TIME_ZONE, false)
            .configure(WRITE_DATES_AS_TIMESTAMPS, false);

    @Override
    public EmailRequestVisited deserialize(byte[] message) throws IOException {
        return objectMapper.readValue(message, EmailRequestVisited.class);
    }

    @Override
    public boolean isEndOfStream(EmailRequestVisited nextElement) {
        return false;
    }

    @Override
    public TypeInformation<EmailRequestVisited> getProducedType() {
        return TypeInformation.of(EmailRequestVisited.class);
    }
}
