package com.atlassian.product.fabric.notifications.producer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.guava.GuavaModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;

import java.util.Map;
import java.util.UUID;

import static com.fasterxml.jackson.databind.DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE;
import static com.fasterxml.jackson.databind.SerializationFeature.WRITE_DATES_AS_TIMESTAMPS;

public class NPLogRequestGeneratorRandom extends NPLogRequestGeneratorLinear {

    private static ObjectMapper objectMapper = new ObjectMapper()
            .registerModule(new JavaTimeModule())
            .registerModule(new GuavaModule())
            .configure(ADJUST_DATES_TO_CONTEXT_TIME_ZONE, false)
            .configure(WRITE_DATES_AS_TIMESTAMPS, false);

    public static void main(String[] args) throws JsonProcessingException, InterruptedException {


        Map<String, Object> configs = getKafkaConfig();


        Producer<String, String> producer = new KafkaProducer<>(configs);

        int msgs = 100000;
        int pauseSessionAtEvery = 30;

        for (int i = 0; i < msgs; i++) {
            int issueId = (int) (Math.random() * 3);
            int recipientId = (int) (Math.random() * 3);

            String emailRequestId = UUID.randomUUID().toString();

            producer.send(getProducerRecord(issueId, recipientId, emailRequestId));

            if (i % pauseSessionAtEvery == 0) {
                producer.flush();
                Thread.sleep(2500);
            }

            producer.flush();
            Thread.sleep(20);
        }

        producer.flush();
        producer.close();
    }


}
